from tflite_runtime.interpreter import Interpreter 
from io import BytesIO, StringIO
import numpy as np
import time
import os
import time
import sys
import base64
import platform
import tflite_utils
import api_pipe
import requests
import image_tools

def getAsBase64(url):
  return base64.b64encode(requests.get(url).content)

def testModel(image,modelFile,labels):
  print("testing model",modelFile)   
  interpreter = Interpreter("models/"+modelFile)
  interpreter.allocate_tensors()

  threshold = 0.4
  api_pipe.printInfo(0,"ready")
  try:
    start = time.perf_counter()
    objs = tflite_utils.cvScaleInference(interpreter, image, threshold)
    inference_time = time.perf_counter() - start

    output = []
    for obj in objs:
      result = {
        'bbox': [obj.bbox.xmin,obj.bbox.ymin,obj.bbox.xmax,obj.bbox.ymax],
        'class': labels[obj.id],
        'score': obj.score
      }
      output.append(result)
    #outputted data is based on original feed in image size
    api_pipe.printData(0,output, int(inference_time * 1000))
  except Exception as e:
    api_pipe.printError(0,str(e))

def main():
  labels = api_pipe.loadLabels("models/ssd_coco_labels.txt")
  line = getAsBase64("https://cdn.shinobi.video/images/people/brothers%20in%20the%2090s.jpg")
  image = image_tools.cvImageFromBase64(line)
  
  models = os.listdir('models')
  for f in models:
    if os.path.splitext(f)[1] == ".tflite":
      testModel(image,f,labels)

if __name__ == '__main__':
    main()
