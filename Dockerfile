FROM python:3.7-buster
#FROM python:3.9-buster

RUN apt update -y
ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /usr/src/app

#additinal (and optional?) packages and updates
RUN apt install -y cmake

#deps for opencv
RUN apt install -y libwayland-cursor0 libxfixes3 libva2 libx264-155 libavutil56 libxcb-render0 libwavpack1 libvorbis0a libcroco3 libxinerama1 libva-x11-2 libtiff5 libpixman-1-0 libwayland-egl1 libzvbi0 libxkbcommon0 libatk-bridge2.0-0 libmp3lame0 libjbig0 libxcb-shm0 libspeex1 libwebpmux3 libatlas3-base libpangoft2-1.0-0 libogg0 libgraphite2-3 libsoxr0 libatspi2.0-0 libdatrie1 libswscale5 libharfbuzz0b libbluray2 libwayland-client0 libaom0 libopus0 libxvidcore4 libgsm1 libxcursor1 libavformat58 libswresample3 libssh-gcrypt-4 libgdk-pixbuf2.0-0 libxdamage1 libsnappy1v5 libdrm2 libxcomposite1 libgtk-3-0 libepoxy0 libgfortran5 libvorbisenc2 libopenmpt0 libvdpau1 libchromaprint1 libavcodec58 libcairo-gobject2 libxrender1 libgme0 libpango-1.0-0 libtwolame0 libcodec2-0.8.1 libcairo2 libatk1.0-0 libxrandr2 libwebp6 librsvg2-2 libopenjp2-7 libpangocairo-1.0-0 libshine3 libxi6 libvorbisfile3 libx265-165 libmpg123-0 libthai0 libva-drm2 libtheora0 libfontconfig1 libvpx5

#deps for scipy
#RUN apt install libatlas3-base libgfortran5

RUN pip3 install --upgrade pip setuptools wheel

#install python dependencies
#we install whl due to comilation problems with opencv (cmake test fail)
#COPY whl/*.whl ./whl/
#RUN pip3 install whl/*.whl

COPY requirements.txt ./
RUN pip3 install --no-cache-dir -r requirements.txt

#install node and dependencies
RUN apt install -y sudo
RUN curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash -
RUN apt -y install nodejs
RUN npm install pm2 -g
COPY package.json ./
RUN npm install --unsafe-perm

EXPOSE 8082

#copy and prepare program files
COPY . .

RUN chmod -f +x /usr/src/app/init.sh
RUN chmod -f +x /usr/src/app/pm2.yml

ENTRYPOINT ["/usr/src/app/init.sh"]
CMD [ "pm2-docker", "/usr/src/app/pm2.yml" ]
