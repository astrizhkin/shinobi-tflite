from tflite_runtime.interpreter import Interpreter 
import numpy as np
import cv2
import time
import os
import argparse
import json
import time
import sys
import base64
import platform
import tflite_utils
import image_tools
import api_pipe
import image_diff_cv
import traceback


def main():
  args = sys.argv[1:]
  if len(args) != 2:
    api_pipe.printError("invalid aruments")

  labels = api_pipe.loadLabels(args[0])
  interpreter = Interpreter(args[1])
  interpreter.allocate_tensors()
  inputW,inputH = tflite_utils.input_size(interpreter)
  api_pipe.printInfo(0,"ready")
  while True:
    line = sys.stdin.readline().rstrip("\n")
    messageId = None
    try:
      message = json.loads(line)
      messageId = message['msgId']
      threshold = float(message['pythonObjectThreshold'])
      objectDetectionType = message['pythonObjectDetectionType']
      #print("Object threshold "+str(threshold))
      image = image_tools.cvImageFromBase64(message['image'])
      prevImageStr = message['prevImage']
      previousImage = None
      if prevImageStr is not None:
        previousImage = image_tools.cvImageFromBase64(prevImageStr)

      startTime = time.perf_counter()
      diffImage,totalDiff,autoThreshold = image_diff_cv.imageDiff2(previousImage,image,
                                                                   message['pythonThresholdType'],
                                                                   int(message['pythonDiffPixelThreshold']),
                                                                   int(message['autoMinPixelDiffThreshold']),
                                                                   int(message['autoMaxPixelDiffThreshold']),
                                                                   int(message['pythonAdaptiveThresholdWindowSize']),
                                                                   int(message['pythonAdaptiveThresholdC']),
                                                                   int(message['pythonDiffGaussianFilter']),
                                                                   int(message['pythonDiffErodeFilter']))

      infStartTime = time.perf_counter()

      if objectDetectionType=='crop':
        cropBox = image_diff_cv.maxDiffBBox(diffImage, int(inputW), int(inputH))
        objs = tflite_utils.cvCropInference(interpreter, image, cropBox, threshold/100)
      elif objectDetectionType=='scale':
        h, w = image.shape[:2]
        cropBox = tflite_utils.BBox(xmin=0, ymin=0, xmax=int(w), ymax=int(h))
        objs = tflite_utils.cvScaleInference(interpreter, image, threshold/100)
      else:
        raise Exception('Unknown object detection '+str(objectDetectionType))    

      infEndTime = time.perf_counter()

      output = []
      for obj in objs:
        diffScore, area = image_diff_cv.countDiffInBBox2(diffImage,obj.bbox,128)
        result = {
          'bbox': [obj.bbox.xmin,obj.bbox.ymin,obj.bbox.xmax,obj.bbox.ymax],
          'class': labels[obj.id],
          'score': round(obj.score*100,1),
          'diff': round(diffScore*100,1),
          'area' : round(area*100,2)
        }
        output.append(result)
      #outputted data is based on original feed in image size
      diffImageAsString = None
      if message['pythonDiffImageOut']:
        #make 4 channel image from 1 channel (2 chennel is not supported in opencv)
        diffImage = cv2.merge((diffImage,diffImage,diffImage,diffImage))
        _,diffImageBytes = cv2.imencode('.webp', diffImage)
        diffImageb64 = base64.b64encode(diffImageBytes)
        diffImageAsString = diffImageb64.decode('ascii')

      infTime = infEndTime - infStartTime
      diffTime = (time.perf_counter() - infEndTime) + (infStartTime - startTime)
      h, w = image.shape[:2]
      api_pipe.printDataWithDiff(messageId,
                                 output, 
                                 cropBox,w,h,
                                 int(infTime * 1000),
                                 int(diffTime * 1000),
                                 round(totalDiff*100, 3),
                                 autoThreshold,
                                 diffImageAsString)
    except Exception as e:
      fullMessage = traceback.format_exc()
      errorFile = open("err_"+str(time.perf_counter())+".log", "w")
      errorFile.write(line + "\n" + fullMessage)
      errorFile.close()
      api_pipe.printError(messageId,fullMessage)

if __name__ == '__main__':
    main()
