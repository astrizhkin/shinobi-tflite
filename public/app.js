$(function () {
    var includes = $('[data-include]');
    $.each(includes, function () {
      var file = $(this).data('include') + '.html';
      $(this).load(file);
    })
});

var waitForEl = function(selector, callback) {
    if (jQuery(selector).length) {
      callback();
    } else {
      setTimeout(function() {
        waitForEl(selector, callback);
      }, 100);
    }
  };

function drawImage(drawContext, b64data, width, height, onReadyCall) {
    var img = new Image();
    img.src = "data:image/webp;base64," + b64data;
    img.onload = function () {
        drawContext.drawImage(img, 0, 0, width, height);
        onReadyCall();
    };
}

function mapCoord(imageCoord, htmlSize, imageSize) {
    if (!imageSize) {
        imageSize = htmlSize;
    }
    var htmlCoord = Math.round(imageCoord * htmlSize / imageSize);
    return htmlCoord;
}

function prepareData(frameObject,canvasWidth,canvasHeight) {
    if(frameObject.detBox) {
    frameObject.detBox[0] = mapCoord(frameObject.detBox[0],canvasWidth, frameObject.imgWidth);
    frameObject.detBox[1] = mapCoord(frameObject.detBox[1],canvasHeight, frameObject.imgHeight);
    frameObject.detBox[2] = mapCoord(frameObject.detBox[2],canvasWidth, frameObject.imgWidth);
    frameObject.detBox[3] = mapCoord(frameObject.detBox[3],canvasHeight, frameObject.imgHeight);
    }
    for (const object of frameObject.data) {
        object.bbox[0] = mapCoord(object.bbox[0],canvasWidth, frameObject.imgWidth);
        object.bbox[1] = mapCoord(object.bbox[1],canvasHeight, frameObject.imgHeight);
        object.bbox[2] = mapCoord(object.bbox[2],canvasWidth, frameObject.imgWidth);
        object.bbox[3] = mapCoord(object.bbox[3],canvasHeight, frameObject.imgHeight);
    
        const bbox = object.bbox
        const clazz = object.class
        const score = object.score
        const diff = object.diff
        const area = object.area
        object.classColor = "gray";
        object.eventColor = "gray";
        object.scoreColor = "gray";
        object.diffColor = "gray";
        object.areaColor = "gray";
        if (object.eventPass) {
            object.eventColor = "red";
            if (object.classPass) {
                object.classColor = "white";
            }
            if (object.scorePass) {
                object.scoreColor = "white";
            }
            if (object.diffPass) {
                object.diffColor = "white";
            }
            if (object.areaPass) {
                object.areaColor = "white";
            }
        }else{
            if (object.classPass) {
                object.classColor = "red";
            }
            if (object.scorePass) {
                object.scoreColor = "red";
            }
            if (object.diffPass) {
                object.diffColor = "red";
            }
            if (object.areaPass) {
                object.areaColor = "red";
            }
        }
    }
}

function renderEvent(frameObject, camRowId) {
    var canvas = $("#" + camRowId + " canvas")[0]
    prepareData(frameObject, canvas.width, canvas.height);
    console.log("frame " + frameObject.camid + " display in " + camRowId);
    //fill info table
    var tdCells = $("#" + camRowId + " .camInfoTable td");
    tdCells = tdCells.filter(function (i) {
        const el = $(this);
        return el.attr('data-field') != "";
    });
    tdCells.each(function (i) {
        const el = $(this);
        const dataAttr = el.attr('data-field');
        el.text(Math.round(frameObject[dataAttr] * 10) / 10);
    });

    //fill object table
    var objRowTemplate = $('#objRowTemplate')
    var objectTableData = $("#" + camRowId + " .objTable tbody");
    objectTableData.empty()
    for (const object of frameObject.data) {
        const row = objRowTemplate.clone();
        row.removeAttr('style');

        if(object.eventPass) {
            const dataAttr = row.attr('data-field');
            row.attr('style', 'background:' + object[dataAttr + 'Color'])
        }
        row.children().each(function () {
            const el = $(this);
            const dataAttr = el.attr('data-field');
            el.attr('style', 'color:' + object[dataAttr + 'Color'])
            el.text(object[dataAttr]);
        })
        row.appendTo(objectTableData);
    }

    //fill canvas
    if (canvas.getContext) {
        var ctx = canvas.getContext('2d');
        ctx.font = "14px serif";
        drawImage(ctx, frameObject.frame, canvas.width, canvas.height, () => {
            drawImage(ctx, frameObject.diffImage, canvas.width, canvas.height, () => {
                if(frameObject.detBox) {
                    ctx.strokeStyle = 'yellow';
                    ctx.beginPath();
                    ctx.rect(frameObject.detBox[0], frameObject.detBox[1], frameObject.detBox[2] - frameObject.detBox[0], frameObject.detBox[3] - frameObject.detBox[1])
                    ctx.stroke();
                }
                for (const object of frameObject.data) {
                    const bbox = object.bbox

                    ctx.fillStyle = object.eventColor;
                    ctx.strokeStyle = object.eventColor;
                    ctx.beginPath();
                    ctx.rect(bbox[0], bbox[1], bbox[2] - bbox[0], bbox[3] - bbox[1])
                    ctx.stroke();
                    ctx.fillStyle = object.classColor;
                    ctx.strokeStyle = object.classColor;
                    ctx.fillText(object.class, bbox[0], bbox[1] - 2);
                    ctx.fillStyle = object.scoreColor;
                    ctx.strokeStyle = object.scoreColor;
                    ctx.fillText("" + object.score, bbox[0] + ctx.measureText(object.class).width + 5, bbox[1] - 2);
                    ctx.fillStyle = object.diffColor;
                    ctx.strokeStyle = object.diffColor;
                    ctx.fillText("" + object.diff, bbox[0] + ctx.measureText(object.class).width + 5 + ctx.measureText("" + object.score).width + 5, bbox[1] - 2);
                }
            })
        })
    }
}
