# Plugin

Shinobi Object Detection Pluging based on Tensorflow Lite library.
Primary goal of the plugin is to acheive accpetable performance (both time and quality) of detection on plain ARM based CPU

# Supported Platforms

Tensorflow Lite works on x86_64, armv71 and aarch64(TODO: needs testing) architectures

# Shinobi version

Compatible with Shinobi master branch on Feb 2023

# Models and performance

Tensorflow Lite can run on CPU only with acceptable performance

Plugin tested with 4 coco dataset trained models:
- ssd mobilenet v3 small 320x320 input
- ssd mobilenet v3 large 320x320 input
- ssdlite mobiledet qat 320x320 input
- ssdlite mobiledet cpu 320x320 input
<a href="https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf1_detection_zoo.md#mobile-models">Read more</a>
<a href="https://coral.ai/models/all/">Read more</a>

https://gilberttanner.com/blog/tflite-model-maker-object-detection/

TF Model Zoo
https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_detection_zoo.md

Few Shot Learning
https://github.com/tensorflow/models/blob/master/research/object_detection/colab_tutorials/eager_few_shot_od_training_tf2_colab.ipynb

Carefully select model accoring to performance you would like to acheive

Reference inference time measured for single image on single CPU core

on Raspberry Pi 4 at 1850MHz (Raspberry Pi OS, buster, 32bit)
| Model | mAP | Time(ms) |
| --- | --- | --- |
| ssd mobilenet v3 small | 15.4 | 90 |
| ssd mobilenet v3 large | 22.6 | 280 |
| ssdlite mobiledet qat | 32.9 | 280 |
| ssdlite mobiledet cpu | 24 | N/A |

on Intel i7-8550u (Ubuntu, buster, 64bit)
| Model | mAP | Time(ms) |
| --- | --- | --- |
| ssd mobilenet v3 small | 15.4 | 20 |
| ssd mobilenet v3 large | 22.6 | 40 |
| ssdlite mobiledet qat | 32.9 | 600 |
| ssdlite mobiledet cpu | 24 | 50 |


# In practice

It is make sense to setup object detection on low quality CCTV camera stream like 640x480 with 1-2 fps
TODO: For unknown reason optimal (in theory) 320x320 stream do not provide any perfromance improvement despite of no need in image reside using Pillow library

Object detection eliminates false positives of traditional motion detection algorithms especially in outdoor environments
Read more about object detection in shinobi documentation

# Installing the Plugin

The pluging is docker based

1. checkout repository
	<code>git clone https://gitlab.com/astrizhkin/shinobi-tflite</code>

2. build shinobi-tflite docker image
	<code>sudo docker build -t shinobi-tflite .</code>

3. modify shinobi main configuration file (conf.json) to accept plugin key

4. run docker image
	Pass environment variables if needed: PLUGIN_KEY, SHINOBI_HOST, SHINOBI_PORT
	Default values are: 
		PLUGIN_KEY="tflitekey"
		SHINOBI_HOST="localhost"
		SHINOBI_PORT=8080

	<code>sudo docker run -it --rm --name shinobi-tflite-run shinobi-tflite</code>

# Full docker-compose.yml example

<pre><code>
version: "3"
services:
    shinobi:
        image: shinobi-image:1.0
        container_name: shinobi
        environment:
           - 'PLUGIN_KEYS={"TensorflowLite": "tflitekey"}'
           - SSL_ENABLED=false
        volumes:
           - ./config:/config
           - ./customAutoLoad:/home/Shinobi/libs/customAutoLoad
           - ./database:/var/lib/mysql
           - ./videos:/home/Shinobi/videos
           - ./plugins:/home/Shinobi/plugins
           - /dev/shm/Shinobi/streams:/dev/shm/streams
        ports:
           - 8081:8080
        restart: unless-stopped
        networks:
           - shinobi
    shinobi-tflite:
        image: shinobi-tflite
        container_name: shinobi-tflite
        depends_on:
           - shinobi
        environment:
           - PLUGIN_KEY=tflitekey
           - SHINOBI_HOST=shinobi
           - SHINOBI_PORT=8080
        ports:
           - 8082:8082
        restart: unless-stopped
        networks:
           - shinobi
networks:
  shinobi:
    driver: bridge
</code></pre>

# Extended settings

<pre><code>
{
  "plug":"TensorflowLite",
  "host":"localhost",
  "port":8080,
  "hostPort":8082,
  "key":"change_this_to_something_very_random",
  "mode":"client",
  "type":"detector",
  "enabled": true,
  "debugLog": true,
  "objectThreshold": 65,
  "numDetectorPrcesses": 2,
  "rejectAwitTime": 750,
  "labelsFile": "models/ssd_coco_labels.txt",
  "modelFile": "models/ssdlite_mobiledet_coco_qat_postprocess.tflite"
}
</code></pre>


# Under the hood

Shinobi Thensorflow Lite plugin is
- a node.js process that 
- connects to main Shinobi application and
- receives stream images
- encodes them to base64 format 
- pass strings to stdin of non-busy python process from the process pool which
-    decodes and open images with Pillow
-    resizes them to 320x320
-    run inference using Tensorflow Lite python library
-    encodes results as JSON message and 
-    pass back to stdout
- node.js process converts result to shinobi event message format and 
- pass results back to shinobi for further prcessing

The plugin is based on original Shinobi Tensorflow-Coral plugin which requires external Google Coral accelerator

# TODO

Please feel free to contact me to share ideas how to improve quality and performance of the plugin.
