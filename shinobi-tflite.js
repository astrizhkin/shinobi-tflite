// Base Init >>
var fs = require('fs');
var config = require('./conf.json')
var dotenv = require('dotenv').config()
var express = require('express');
//var sharp = require('sharp');
const { sse , sseHub, Hub } = require('@toverux/expresse');
const currentDirectory = process && process.cwd ? process.cwd() + '/' : __dirname + '/'
var s
const {
    workerData
} = require('worker_threads');
if (workerData && workerData.ok === true) {
    try {
        s = require('./pluginWorkerBase.js')(__dirname, config)
    } catch (err) {
        console.log(err)
        try {
            s = require('../pluginWorkerBase.js')(__dirname, config)
        } catch (err) {
            console.log(err)
            return console.log(config.plug, 'WORKER : Plugin start has failed. pluginBase.js was not found.')
        }
    }
} else {
    try {
        s = require('./pluginBase.js')(__dirname, config)
    } catch (err) {
        console.log(err)
        try {
            s = require('../pluginBase.js')(__dirname, config)
        } catch (err) {
            console.log(err)
            return console.log(config.plug, 'Plugin start has failed. pluginBase.js was not found.')
        }
    }
}

const spawn = require('child_process').spawn;
function debugLog(...args) {
    if (config.debugLog === true) console.log(...args)
}

class PythonWorker {
    child = null;
    ready = false;
    name = null;

    constructor(name) {
        this.name = name;
        respawn(this);
    }
}

function cameraConfig(camId) {
    var allConfig = config.allCamConfig;
    if(!config.camConfig){
        config.camConfig = new Map();
    }
    if (!config.camConfig.get(camId)){
        config.camConfig.set(camId,{});
    }
    var result = Object.assign({}, allConfig);
    result = Object.assign(result, config.camConfig.get(camId));
    return result;
}

function putCamProperty(camId, property, value) {
    var camConfig = config.camConfig.get(camId);
    camConfig[property] = value;
}

function removeCamProperty(camId, property) {
    var camConfig = config.camConfig.get(camId);
    delete camConfig[property];
}


//find -name *.jpg -type f -size +1k -exec cat {} + | ffmpeg -f image2pipe -r 30 -vcodec mjpeg -i - -vcodec libx264 reo3.mp4
//ffmpeg -y -stream_loop 1 -r 1 -i image.png -vcodec libx264 -crf 17 -pix_fmt yuv420p output.ts

function respawn(pythonWorker) {

    debugLog(pythonWorker.name + " respawned python", (new Date()))
    pythonWorker.child = spawn(config.python ? config.python : 'python3', ['-u', currentDirectory + 'detect_motion_image_cv.py',config.labelsFile,config.modelFile]);

    pythonWorker.child.on('exit', () => {
        debugLog(pythonWorker.name + " Python Process Ends")
        pythonWorker.child = respawn(pythonWorker);
    });

    pythonWorker.child.stderr.on('data', function (data) {
        var rawString = data.toString('utf8');
        debugLog(pythonWorker.name + " " + rawString);
    })
    pythonWorker.child.stdout.on('data', function (data) {
        var rawString = data.toString('utf8');
        var messages = rawString.split('\n')
        messages.forEach((message) => {
            if (message === "") return;
            var obj = {}
            try {
                obj = JSON.parse(message)
            } catch(e) {
            }
            if (obj.type === "error") {
                debugLog(pythonWorker.name + " reported inner error: " + message.data);
                //throw message.data;
            } 

            if (obj.type === "info" && obj.data === "ready") {
                debugLog(pythonWorker.name + " set ready true")
                setTimeout(() => {
                    pythonWorker.ready = true;
                }, 2000)
            } else {
                if (obj.type !== "data" && obj.type !== "info") {
                    debugLog(pythonWorker.name + " Unexpected message: " + message);
                    //throw pythonWorker.name + " Unexpected message: " + message;
                }
            }
        })
    })
}

var pythonWorkers = new Array();
for (var i = 0; i < config.numDetectorProcesses; i++) {
    pythonWorkers.push(new PythonWorker("TFLiteWorker" + i));
}

class Queue {
    constructor() { this._items = []; }
    enqueue(item) { this._items.push(item); }
    first() {return this._items[0]; }
    dequeue() { return this._items.shift(); }
    get size() { return this._items.length; }
}


class AutoQueue extends Queue {
    constructor() {
        super();
    }

    enqueue(action,frameInfo) {
        return new Promise((resolve, reject) => {
            super.enqueue({ action, frameInfo,resolve, reject });
            this.dequeue();
        });
    }

    async dequeue() {
        var readyWorker = null;
        for (const pythonWorker of pythonWorkers) {
            if (pythonWorker.ready) {
                readyWorker = pythonWorker;
                break;
            }
        }
        if (!readyWorker) return false;

        let item = super.dequeue();

        if (!item) return false;

        try {
            readyWorker.ready = false;
            let resp = await item.action(readyWorker,item.frameInfo)
            item.resolve(resp);
        } catch (e) {
            item.reject({pythonWorker:readyWorker,e});
        } finally {
            readyWorker.ready = true;
            this.dequeue();
        }

        return true;
    }
}
var autoQueue = new AutoQueue();
// Base Init />>

var previousFrames = new Map();

class FrameInfo {
    constructor(frame_b64, id,timestamp){
        this.frame_b64 = frame_b64;
        this.id = id;
        this.timestamp = timestamp;
    }
}

class Statistics {
    constructor(name,period) {
        this._name = name;
        this._timestamps = new Array();
        this._period = period;
        this._mean = 0;
        this._summ = 0;
        this._lastSpike = 0;
    }

    addVal(val, timestamp) {
        timestamp = timestamp.getTime();
        while((this._timestamps[0]+this._period)<timestamp){
            this._timestamps.shift();
        }
        if(isNaN(this._mean)){
            this._mean = val;
        }
        if(val > this._mean) {
            this._lastSpike = val;
        }
        this._summ = this._mean * this._timestamps.length + val;
        this._timestamps.push(timestamp);
        this._mean = this._summ / this._timestamps.length;
        return this._mean;
    }

    sum(){
        return this._sum;
    }

    mean(){
        return this._mean;
    }

    lastSpike(){
        return this._lastSpike;
    }
}

class AdaptiveThresholdStatistics extends Statistics {
    constructor(name,period,initialThreshold){
        super(name,period)
        this._currentThreshold = initialThreshold;
        this._lastUpdate = 0;
    }

    updateThreshold(minThreshold,maxThreshold,keepBelowTreshold,keepAboveThreshold) {
        const lastValueTimestamp = this._timestamps[this._timestamps.length-1];
        if(this._lastUpdate+this._period < lastValueTimestamp) {
            if(this.mean() > keepBelowTreshold) {
                this._currentThreshold++;
                this._currentThreshold = Math.min(this._currentThreshold,maxThreshold);
            }else if(this.mean() < keepAboveThreshold) {
                this._currentThreshold--;
                this._currentThreshold = Math.max(this._currentThreshold,minThreshold);
            }
            this._lastUpdate = lastValueTimestamp;
        }
        return this._currentThreshold;
    }

    currentThreshold() {
        return this._currentThreshold;
    }
}

const stat = new Map();
const adaptiveThresholdStat = new Map();

function getStat(camid,name) {
    return _getStat(camid,name,stat,function(name) {
        return new Statistics(name, config.statisticsAggregationPeriod);
    });
}

function getAdaptiveThresholdStat(camid,statisticsPeriod,initialThreshold) {
    return _getStat(camid,"adaptiveThreshold",adaptiveThresholdStat,function(name) {
        return new AdaptiveThresholdStatistics(name, statisticsPeriod, initialThreshold);
    });
}

function _getStat(camid,name,map,statFactory) {
    var camStat = map.get(camid);
    if (!camStat) {
        camStat = new Map();
        map.set(camid, camStat);
    }
    var nameStat = camStat.get(name);
    if(!nameStat) {
        nameStat = statFactory(name);
        camStat.set(name, nameStat);
    }
    return nameStat;
}

function objectFilterPass(objectClass,objectFilterList) {
    const objectFilterArray = objectFilterList.split(',');
    for (const objectFilter of objectFilterArray) {
        if(objectClass == objectFilter.trim()){
            return false;
        }
    }
    return true;
}

const emptyDataObject = { data: [], type: undefined, time: 0 };
var globalMessageId = 0;

s.detectObject = function (buffer, d, tx, frameLocation, callback) {
    var currentFrame = new FrameInfo(buffer.toString('base64'),d.id,new Date())
    const camConfig = cameraConfig(currentFrame.id)
    
    autoQueue.enqueue(async (pythonWorker) => {
        var startProcessTime = new Date()
        var queueTime = startProcessTime - currentFrame.timestamp;
        if (queueTime > config.rejectAwitTime) {
            throw new Error(d.id +" await time limit exceeded "+queueTime);
        }
        var messageId = globalMessageId++;
        var message = {
            msgId: messageId,
            image: currentFrame.frame_b64,
            //prevImage: null,
            prevImage: previousFrames.get(currentFrame.id)!=null ? previousFrames.get(currentFrame.id).frame_b64 : null
        }
        Object.assign(message,camConfig);
        var jsonMessage = JSON.stringify(message);
        //debugLog(pythonWorker.name +" Send "+d.id+" message "+messageId)
        pythonWorker.child.stdin.write(jsonMessage + '\n');
    
        var startPythonTime = new Date();
        var responseMessage = null;
        await new Promise((resolve) => {
            pythonWorker.child.stdout.once('data', (data) => {
                var rawString = data.toString('utf8').split("\n")[0];
                try {
                    responseMessage = JSON.parse(rawString)
                } catch (e) {
                    debugLog(pythonWorker.name + " got malformed data [" + rawString + "]")
                    responseMessage = { data: [] };
                }
                resolve();
            });
        })
        const responseMessageId = responseMessage.msgId
        if(responseMessageId != messageId) {
            throw new Error("with wrong message id " + responseMessageId + " expected " + messageId)
        } 
        const endTime = new Date();
        const prepareTime = startPythonTime - startProcessTime;
        const processingTime = endTime - startPythonTime;
        const fullTime = endTime - currentFrame.timestamp;
        //var diffImageBytes = Buffer.from(message.diffImage,'base64');
    
        debugLog(pythonWorker.name + " Results id=" + currentFrame.id + " %j FullTime=" + fullTime + " InQueue=" + queueTime + " Python=" + processingTime + "(Inf=" + responseMessage.infTime +"+Diff="+responseMessage.diffTime+")", responseMessage.data);
        Object.assign(responseMessage, { time: fullTime,processingTime,queueTime});
        return responseMessage;
    }).then((resp) => {
        const mean_time = getStat(currentFrame.id,"time").addVal(resp.time, currentFrame.timestamp);
        const mean_infTime = getStat(currentFrame.id,"infTime").addVal(resp.infTime, currentFrame.timestamp);
        const mean_queueTime = getStat(currentFrame.id,"queueTime").addVal(resp.queueTime, currentFrame.timestamp);
        const mean_diffTime = getStat(currentFrame.id,"diffTime").addVal(resp.diffTime, currentFrame.timestamp);
        const mean_autoThreshold = getStat(currentFrame.id,"autoThreshold").addVal(resp.autoThreshold, currentFrame.timestamp);
        const mean_totalDiff = getStat(currentFrame.id,"totalDiff").addVal(resp.totalDiff, currentFrame.timestamp);
        const spike_time = getStat(currentFrame.id,"time").lastSpike();
        const spike_infTime = getStat(currentFrame.id,"infTime").lastSpike();
        const spike_queueTime = getStat(currentFrame.id,"queueTime").lastSpike();
        const spike_diffTime = getStat(currentFrame.id,"diffTime").lastSpike();
        const spike_autoThreshold = getStat(currentFrame.id,"autoThreshold").lastSpike();
        const spike_totalDiff = getStat(currentFrame.id,"totalDiff").lastSpike();

        var camAdaptiveThresholdStat = getAdaptiveThresholdStat(currentFrame.id,camConfig.timeAdaptiveThresholdStatisticsPeriod,camConfig.pythonDiffPixelThreshold);
        camAdaptiveThresholdStat.addVal(resp.totalDiff, currentFrame.timestamp);
        const timeAdaptiveThreshold = camAdaptiveThresholdStat.updateThreshold(camConfig.autoMinPixelDiffThreshold,camConfig.autoMaxPixelDiffThreshold,camConfig.timeAdaptiveThresholdAggregationTotalDiffKeepBelow,camConfig.timeAdaptiveThresholdAggregationTotalDiffKeepAbove);
        if(camConfig.timeAdaptiveEnable === 'true') {
            putCamProperty(currentFrame.id,"pythonDiffPixelThreshold",timeAdaptiveThreshold);
        }else{
            removeCamProperty(currentFrame.id,"pythonDiffPixelThreshold");
        }

        var results = resp.data;
        var mats = []
        if (Array.isArray(results) && results[0]) {
                results.forEach(function (v) {
                var areaPenalty = v.area;
                //the samller object is the higher score must be
                //10% area maps score 1:1 
                v.scorePass = v.score > camConfig.objectScoreThreshold;
                v.diffPass = v.diff > camConfig.objectDiffThreshold;
                v.areaPass = v.area > camConfig.objectAreaBelowRejectThreshold;
                v.classPass = objectFilterPass(v.class,camConfig.objectRejectFilter);
                v.eventPass = v.classPass 
                                && v.scorePass 
                                && v.diffPass 
                                && v.areaPass
                                && resp.totalDiff < camConfig.diffImageAboveRejectThreshold;
                v.eventConfidence = Math.pow(v.score * v.diff * v.area, 1/3);
            });
            results.forEach(function (v) {
                if (v.eventPass) {
                    debugLog("Object " + v.class + " event pass");
                        mats.push({
                            x: v.bbox[0],
                            y: v.bbox[1],
                            width: (v.bbox[2] - v.bbox[0]),
                            height: (v.bbox[3] - v.bbox[1]),
                            tag: v.class,
                        confidence: v.eventConfidence,
                        })
                    }
                })
                if (Array.isArray(mats) && mats[0]) {
                    var isObjectDetectionSeparate = d.mon.detector_pam === '1' && d.mon.detector_use_detect_object === '1';
                    var width = parseFloat(isObjectDetectionSeparate && d.mon.detector_scale_y_object ? d.mon.detector_scale_y_object : d.mon.detector_scale_y);
                    var height = parseFloat(isObjectDetectionSeparate && d.mon.detector_scale_x_object ? d.mon.detector_scale_x_object : d.mon.detector_scale_x);
                    tx({
                        f: 'trigger',
                        id: d.id,
                        ke: d.ke,
                        details: {
                            plug: config.plug,
                            name: 'Tensorflow',
                            reason: 'object',
                            matrices: mats,
                            imgHeight: width,
                            imgWidth: height,
                            time: resp.time
                        },
                        frame: buffer
                    })
                }
        }
        Object.assign(resp,{
            camid: d.id,
            timestamp: currentFrame.timestamp,
            frame: currentFrame.frame_b64,
            mean_time,
            mean_queueTime,
            mean_infTime,
            mean_diffTime,
            mean_autoThreshold,
            mean_totalDiff,
            timeAdaptiveThreshold,
            spike_time,
            spike_queueTime,
            spike_infTime,
            spike_diffTime,
            spike_autoThreshold,
            spike_totalDiff
        })
        getEventHub(d.id).event('frame', resp);
        if(config.saveEvents && Array.isArray(mats) && mats[0]) {
            writeEvent(resp,buffer,previousFrames.get(currentFrame.id)!=null ? previousFrames.get(currentFrame.id).frame_b64:null);
        }

        previousFrames.set(d.id,currentFrame);
        callback();
    }).catch((resp)=>{
        if(resp instanceof Error) {
            debugLog("Unexpected error "+resp);
        }else{
            debugLog(resp.pythonWorker.name + " Task "+d.id+" rejected " + resp.e.message);
        }
        callback();
    })
}

function writeEvent(jsonObject,imageBuffer,prevImageBase64) {
    // stringify JSON Object
   
    var date = jsonObject.timestamp.toISOString().split('T')[0];
    var dir = config.eventOutputDir+"/"+jsonObject.camid+"/"+date+"/"
    fs.mkdir(dir, { recursive: true }, (err) => {
        if (err) throw err;
      });
    var fullJsonObjectWithPrev = {};
    Object.assign(fullJsonObjectWithPrev,jsonObject);
    Object.assign(fullJsonObjectWithPrev,{"prevFrame": prevImageBase64});

    var objects = "";
    jsonObject.data.forEach(function (v) {
        if(v.eventPass) {
            objects+="-"+v.class;
        }
    });
    var jsonContent = JSON.stringify(fullJsonObjectWithPrev);
    fs.writeFile(dir+jsonObject.timestamp.getTime()+objects+".json", jsonContent, 'utf8', function (err) {
        if (err) throw err;
    });
    fs.writeFile(dir+jsonObject.timestamp.getTime()+objects+".jpg",imageBuffer, function (err) {
        if (err) throw err;
    });
}

var eventHubs = new Map();
function getEventHub(camid){
    var hubInfo = eventHubs.get(camid);
    if(!hubInfo){
        var hub = new Hub();
        var streamPath = '/stream'+eventHubs.size;
        hubInfo = {hub,streamPath};
        eventHubs.set(camid,hubInfo);        
        app.get(streamPath, sseHub( {hub} ), (req, res) => {
            res.sse.event('welcome', 'Welcome!');
        });
    }
    return hubInfo.hub;
}

//WebApplication endpoints

app.use(express.static('public'))
app.use(express.json());

app.get('/camList', (req, res) => {
    var results = new Array();
    for(camid of eventHubs.keys()){
        results.push({camid,streamPath:eventHubs.get(camid).streamPath});
    }
    res.json(Array.from(results));
    res.end();
});

app.get('/config', (req, res) => {
    res.json(config.allCamConfig);
    res.end();
});

app.post('/saveConfig', (req, res) => {
    //res.json(config)
    const newConfig = req.body;
    const oldAllCamConfig = config.allCamConfig;
    for (var prop in newConfig) {
        if (Object.prototype.hasOwnProperty.call(newConfig, prop)) {
            if(oldAllCamConfig[prop] != newConfig[prop]) {
                debugLog("Config changed "+prop+" from ["+oldAllCamConfig[prop]+"] to ["+newConfig[prop]+"]")
                oldAllCamConfig[prop] = newConfig[prop]
            }
        }
    }

    res.end();
});

app.get('/historyCamList', async (req, res) => {
    var results = [];
    try {
        var dirName = config.eventOutputDir+"/";
        const dir = fs.opendirSync(dirName);
        for await (const dirent of dir)
            results.push({camid:dirent.name});
    } catch (err) {
        throw err;
    }

    res.json(results);
    res.end();

});


app.get('/historyDates', async (req, res) => {
    var results = [];
    try {
        var dirName = config.eventOutputDir+"/"+req.query.camid+"/"
        const dir = fs.opendirSync(dirName);
        for await (const dirent of dir)
            results.push({date:dirent.name});
        results.sort((a,b) => (a.date > b.date) ? -1 : ((b.date > a.date) ? 1 : 0));
    } catch (err) {
        throw err;
    }

    res.json(results);
    res.end();
});

app.get('/historyEvents', async (req, res) => {
    var results = [];
    try {
        var dirName = config.eventOutputDir+"/"+req.query.camid+"/"+req.query.date+"/"
        const dir = fs.opendirSync(dirName);
        for await (const dirent of dir) {
            if(dirent.name.endsWith('.json')){
                const pureName = dirent.name.substring(0,dirent.name.lastIndexOf('.'));
                const parts = pureName.split('-');
                const date = parts.shift();
                const objects = parts.shift();
                results.push({date,objects,filename:dirent.name});
            }
        }
        results.sort((a,b) => (a.date > b.date) ? -1 : ((b.date > a.date) ? 1 : 0));
    } catch (err) {
        throw err;
    }

    res.json(results);
    res.end();
});

app.get('/historyEvent', async (req, res) => {
    var jsonObject = null;
    try {
        var jsonFile = config.eventOutputDir+"/"+req.query.camid+"/"+req.query.date+"/"+req.query.filename;
        var buffer = fs.readFileSync(jsonFile,'utf8');
        jsonObject = JSON.parse(buffer.toString());
    } catch (err) {
        throw err;
    }

    res.json(jsonObject);
    res.end();
});







