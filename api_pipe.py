import json
import numpy as np
import sys

def loadLabels(path): # Read the labels from the text file as a Python list.
  with open(path, 'r') as f:
    return [line.strip() for i, line in enumerate(f.readlines())]

def printInfo(messageId,text):
  print(json.dumps({"type": "info", "msgId": messageId,"data": text}))
  sys.stdout.flush()

def printError(messageId,text):
  print(json.dumps({"type": "error", "msgId": messageId,"data": text}))
  sys.stdout.flush()

def printData(messageId,array, time):
  print(json.dumps({"type": "data", "msgId": messageId,"data": array, "infTime": time}))
  sys.stdout.flush()

def printDataWithDiff(messageId, array, cropBox, imgWidth, imgHeight, time, diffTime, totalDiff, autoThreshold, diffImage):
  print(json.dumps({"type": "data",
                     "msgId": messageId,
                     "data": array, 
                     "detBox": [cropBox.xmin,cropBox.ymin,cropBox.xmax,cropBox.ymax],
                     "imgWidth": imgWidth,
                     "imgHeight": imgHeight,
                     "infTime": time, 
                     "diffTime": diffTime, 
                     "totalDiff": totalDiff, 
                     "autoThreshold": autoThreshold, 
                     "diffImage": diffImage}))
  sys.stdout.flush()

