import numpy as np
import cv2
import base64

def cvImageFromBase64(b64data):
   nparr = np.frombuffer(base64.b64decode(b64data), np.uint8)
   img = cv2.imdecode(nparr, cv2.IMREAD_UNCHANGED)
   return img

def normalizeImage(img):
    img = np.asarray(img, dtype='float32')
    img /= 255.0
    img -= 0.5
    img *= 2
    return img
