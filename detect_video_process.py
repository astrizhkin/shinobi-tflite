import cv2
from tflite_runtime.interpreter import Interpreter 
import numpy as np
import time
import os
import time
import sys
import platform
import tflite_utils
import api_pipe
import image_diff_cv

def box_image(bbox,image):
  return image.crop((bbox.xmin, bbox.ymin, bbox.xmax, bbox.ymax))

def main():
  labels = api_pipe.loadLabels("models/ssd_coco_labels.txt")
  #classify_labels = common_api.load_labels("models/mobilenet-labels-simple.txt")

  #interpreter = Interpreter("models/lite-model_efficientdet_lite4_detection_default_2.tflite")
  #interpreter = Interpreter("models/ssdlite_mobiledet_coco_qat_postprocess.tflite")
  interpreter = Interpreter("models/ssd_mobilenet_v3_small_coco_2020_01_14.tflite")
  #interpreter = Interpreter("models/ssd_mobilenet_v3_large_coco_2020_01_14.tflite")
  interpreter.allocate_tensors()
  iType = tflite_utils.input_details(interpreter,"dtype")
  inputW,inputH = tflite_utils.input_size(interpreter)

  message = {
    "pythonThresholdType" : "constant",
    "pythonDiffPixelThreshold" : 6,
    "autoMinPixelDiffThreshold" : 5,
    "autoMaxPixelDiffThreshold" : 15,
    "pythonAdaptiveThresholdWindowSize" : 25,
    "pythonAdaptiveThresholdC" : -2,
    "pythonDiffGaussianFilter" : 0,
    "pythonDiffErodeFilter" : 1
  }

#  image_interpreter = Interpreter("models/v3-large_224_1.0_uint8.tflite")
#  image_interpreter = Interpreter("models/v3-large_224_1.0_float.tflite")
#  image_interpreter.allocate_tensors()
#  iType2 = common.input_details(interpreter,"dtype")
#  print("Image input type "+str(iType2))

  threshold=0.5

#  cap = cv2.VideoCapture('video/2022-05-21T16-02-54.mp4')
  #snow
  #cap = cv2.VideoCapture('video/snow/2022-12-10T20-52-33.mp4')

  #shadow motion
  #cap = cv2.VideoCapture('video/false-positives/reo3/2023-04-02T10-31-41.mp4')
  #small person
  #cap = cv2.VideoCapture('video/2023-04-11/hik1/2023-04-11T11-11-45.mp4')
  #cap = cv2.VideoCapture('video/people/2022-10-30T15-18-17.mp4')
  cap = cv2.VideoCapture('video/people/2022-05-21T16-02-54.mp4')
  
  
  #medium person
  #cap = cv2.VideoCapture('video/2023-04-11/hik1/2023-04-11T11-11-10.mp4')
  #cap = cv2.VideoCapture('video/2023-04-11/reo3/2023-04-11T13-28-01.mp4')
  #cap = cv2.VideoCapture('video/2023-04-11/reo3/2023-04-11T11-04-37.mp4')

#  cap = cv2.VideoCapture('video/peeling_banana.mp4')
  count = 0
  prevFrame = None
  while cap.isOpened():
    ret,frame = cap.read()
    if not ret:
        continue

    diffImage,totalDiff,autoThreshold = image_diff_cv.imageDiff2(prevFrame,frame,
                                                                   message['pythonThresholdType'],
                                                                   int(message['pythonDiffPixelThreshold']),
                                                                   int(message['autoMinPixelDiffThreshold']),
                                                                   int(message['autoMaxPixelDiffThreshold']),
                                                                   int(message['pythonAdaptiveThresholdWindowSize']),
                                                                   int(message['pythonAdaptiveThresholdC']),
                                                                   int(message['pythonDiffGaussianFilter']),
                                                                   int(message['pythonDiffErodeFilter']))
    prevFrame = frame

    objs = tflite_utils.cvScaleInference(interpreter, frame, threshold)
    
    for obj in objs:
      label = labels[obj.id]
      confidence = int(obj.score*100)
      diffPercent, area = image_diff_cv.countDiffInBBox2(diffImage, obj.bbox, 128)
      print("Object "+label+"["+str(obj.bbox.xmax-obj.bbox.xmin)+"x"+str(obj.bbox.ymax-obj.bbox.ymin)+"] confidence "+str(confidence)+ " Diff% "+"{:.2f}".format(diffPercent*100))
      cv2.rectangle(frame, (obj.bbox.xmin, obj.bbox.ymin), (obj.bbox.xmax, obj.bbox.ymax), (0,0,255), 1)
      cv2.putText(frame, label + " "+"{:.1f}".format(confidence), (obj.bbox.xmin, obj.bbox.ymin+15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (36,255,12), 1)
      #cv2.putText(frame, label + " "+str(confidence)+" "+classifyLabel+" "+str(classifyConfidence), (obj.bbox.xmin, obj.bbox.ymin+15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (36,255,12), 1)

    cropBox = image_diff_cv.maxDiffBBox(diffImage, int(inputW), int(inputH));
    objs2 = tflite_utils.cvCropInference(interpreter, frame, cropBox, threshold);

    for obj in objs2:
      label = labels[obj.id]
      confidence = int(obj.score*100)
      diffPercent, area = image_diff_cv.countDiffInBBox2(diffImage, obj.bbox , 128)
      print("Object2 "+label+"["+str(obj.bbox.xmax-obj.bbox.xmin)+"x"+str(obj.bbox.ymax-obj.bbox.ymin)+"] confidence "+str(confidence)+ " Diff% "+"{:.2f}".format(diffPercent*100))
      cv2.rectangle(frame, (obj.bbox.xmin, obj.bbox.ymin), (obj.bbox.xmax, obj.bbox.ymax), (0,255,0), 1)
      cv2.putText(frame, label + " "+"{:.1f}".format(confidence), (obj.bbox.xmin, obj.bbox.ymin+15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (36,255,12), 1)

    cv2.rectangle(frame, (cropBox.xmin, cropBox.ymin), (cropBox.xmax, cropBox.ymax), (0,255,255), 1)

    #convert diff image to cv2
    opencvDiffImage = cv2.cvtColor(np.array(diffImage), cv2.COLOR_GRAY2BGR)

    #merge frame with diff image
    frameWithMotion = cv2.addWeighted(frame,0.7,opencvDiffImage,0.3,0)
#    except Exception as e:
      #common_api.printError(str(e))

    count = count + 1
    if cv2.waitKey(10) & 0xFF == ord('q'):
        break
    cv2.imshow('window-name', frameWithMotion)

  cap.release()
  cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
