import numpy as np
import cv2
import micro_scipy
import tflite_utils

def maxDiffBBox(diffImage, boxWidth, boxHeight):
    maxDiffXPoint,maxDiffYPoint = maxDiffPoint(diffImage, boxWidth, boxHeight)
    h, w = diffImage.shape[:2]
    startX = int(min(w-boxWidth,max(0,maxDiffXPoint-boxWidth/2)))
    startY = int(min(h-boxHeight,max(0,maxDiffYPoint-boxHeight/2)))
    endX = startX+boxWidth
    endY = startY+boxHeight
    return tflite_utils.BBox(xmin=startX, ymin=startY, xmax=endX,ymax=endY)

def maxDiffPoint(diffImage, width, height):
    hDiff = np.sum(diffImage,axis = 0)
    hZeros = np.zeros((int(width/2),), dtype=hDiff.dtype)
    hDiff = np.concatenate((hZeros,hDiff,hZeros))
    hMovAvg = moving_average(hDiff,width)
    hLocalMax1 = np.argmax(hMovAvg)
    hLocalMax2 = hMovAvg.shape[0] - np.argmax(np.flip(hMovAvg))
    maxDiffX = (hLocalMax1+hLocalMax2)/2

    vDiff = np.sum(diffImage,axis = 1)
    vZeros = np.zeros((int(height/2),), dtype=hDiff.dtype)
    vDiff = np.concatenate((vZeros,vDiff,vZeros))
    vMovAvg = moving_average(vDiff,height)
    vLocalMax1 = np.argmax(vMovAvg)
    vLocalMax2 = vMovAvg.shape[0] - np.argmax(np.flip(vMovAvg))
    maxDiffY = (vLocalMax1+vLocalMax2)/2
    #print("mov avg diff max "+str((maxDiffX,maxDiffY)));
    return int(maxDiffX),int(maxDiffY)

def imageDiff2(image1, image2,thresholdType='constant',threshold=6,minThreshold=5,maxThreshold=30,adaptiveWindow = 10,adaptiveC = 2,gaussian=0,filter=1):
    if image1 is None:
        image1 = image2
    diffImage = cv2.absdiff(image1,image2)
    diffImage = cv2.cvtColor(diffImage, cv2.COLOR_BGR2GRAY)

    h, w = diffImage.shape[:2]

    if gaussian > 0:
        diffImage = cv2.GaussianBlur(diffImage,(gaussian,gaussian),0)

    if thresholdType == "constant":
        _,diffImage = cv2.threshold(diffImage, threshold, 255, cv2.THRESH_BINARY)
        autoThreshold = threshold
    elif thresholdType == "constantAuto":
        ## computing the histogram of the image
        hist = np.bincount(diffImage.ravel(),minlength=256)
        #hist = cv2.calcHist([diffImage],[0],None,[256],[0,255]).flatten()
        #print("historgram "+str(hist))
        localMinima = micro_scipy.argrelextrema(hist, np.less)[0]
        #print("Local historgram (len="+str(len(localMinima))+") minima "+str(localMinima))
        autoThreshold = threshold
        if len(localMinima) > 0:
            autoThreshold = int(localMinima[0])
            if autoThreshold < minThreshold:
                autoThreshold = minThreshold
            if autoThreshold > maxThreshold:
                autoThreshold = maxThreshold
        _,diffImage = cv2.threshold(diffImage, autoThreshold, 255, cv2.THRESH_BINARY)
    elif thresholdType == "otsu":
        autoThreshold,diffImage = cv2.threshold(diffImage,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    elif thresholdType == "adaptiveMean":
        diffImage = cv2.adaptiveThreshold(diffImage,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,adaptiveWindow,adaptiveC)
        autoThreshold = -1
    elif thresholdType == "adaptiveGaussian":
        diffImage = cv2.adaptiveThreshold(diffImage,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,adaptiveWindow,adaptiveC)
        autoThreshold = -1

    if filter > 0:
        kernel = np.ones((3, 3), np.uint8)
        diffImage = cv2.erode(diffImage, kernel, iterations=filter)
    
    totalDiffPixels = np.sum(diffImage == 255)
    totalDiff = totalDiffPixels/(w * h)
    return diffImage,totalDiff,autoThreshold

def moving_average(a, n) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n

def countDiffInBBox2(diffImage, bbox, threshold):
    # declare output list
    counts = [];
    h, w = diffImage.shape[:2];
    xmin = max(bbox.xmin,0);
    xmax = min(bbox.xmax,w);
    ymin = max(bbox.ymin,0);
    ymax = min(bbox.ymax,h);
    box = diffImage[ymin:ymax, xmin:xmax];
    count = np.sum(box > threshold);
    boxArea = (xmax-xmin)*(ymax-ymin);
    return count/boxArea, boxArea/(w*h);
