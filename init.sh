#!/bin/sh
if [ ! -e "./conf.json" ]; then
    echo "Creating conf.json"
    sudo cp conf.sample.json conf.json
else
    echo "conf.json already exists..."
fi

if [ -n "$ADD_CONFIG" ]; then
  echo ""
else
  if [ -z "$PLUGIN_KEY" ]; then
    PLUGIN_KEY="tflitekey"
  fi
  if [ -z "$SHINOBI_HOST" ]; then
    SHINOBI_HOST="localhost"
  fi
  if [ -z "$SHINOBI_PORT" ]; then
    SHINOBI_PORT="8080"
  fi
  
  ADD_CONFIG='{"host":"'$SHINOBI_HOST'","port":"'$SHINOBI_PORT'","key":"'$PLUGIN_KEY'"}'
fi

node ./modifyConfigurationForPlugin.js TensorflowLite addToConfig=$ADD_CONFIG maxRetryConnection=100

# Test
echo "Test TensorflowLite Python plugin ..."
python3 test.py

# Execute Command
echo "Starting TensorflowLite plugin for Shinobi ..."
exec "$@"